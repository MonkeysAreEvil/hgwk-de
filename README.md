# Description

This is a collection of configuration files, theme files, and packages collated by MonkeysAreEvil to form a coherent modern desktop environment. This repo is designed to work with Funtoo; adjusting for other distros shouldn't be too much work. Mostly intended for Author's personal use; consider this a place to start, or cherry-pick configuration elements.

# Installation

Installation may take a while; there's a lot to download/compile. The `Makefile`s should handle everything; however, there is no error handling. If something goes wrong, you're on your own. It's likely to be something simple, a missing directory or dependency or something. Highly advised to read the `Makefile` closely; there are some steps that may not be entirely desirable.

## Steps (short version)

* Install a base GNU/Linux distro
* Clone the repo somewhere useful e.g. `~/Projects`
* Do a `make install`. If something goes wrong, `make` should give you some useful feedback. Each installation step is separate, so it shouldn't be too hard to pick up from where it failed.

# Licence

GPL v3+
