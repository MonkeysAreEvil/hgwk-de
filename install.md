# Follow handbook instructions up to partitioning. Configure partitions as follows:

    parted -a optimal /dev/sda
    mklabel gpt
    unit mib
    mkpart primary 1 3
    name 1 grub
    set 1 bios_grub on
    mkpart primary 3 250
    name 2 boot
    set 2 boot on
    mkpart primary 250 -1
    print

Partition layout should be:

    | Number | Start   | End     | Size    | File System | Name   | Flags     |
    |--------|---------|---------|---------|-------------|--------|-----------|
    | 1      | 1.00MiB | 3.00MiB | 2.00MiB |             | grub   | bios,grub |
    | 2      | 3.00MiB | 259     | 256MiB  |             | boot   | boot, esp |
    | 3      | 250MiB  | ???MiB  | ???MiB  |             | rootfs |           |

Then, format boot partition:

    mkfs.ext2 /dev/sda2

Then, encrypt and format root filesystem:

    cryptsetup --verify-passphrase luksFormat /dev/sda3
    cryptsetup open /dev/sda3 rootfs
    mkfs.ext4 /dev/mapper/rootfs
    mkdir /mnt/rootfs
    mount /dev/mapper/rootfs /mnt/rootfs

# Configure swap File

	dd if=/dev/zero of=/swap bs=1M count=8192 status=progress
	chown root:root /swap
	chmod 0600 /swap
	mkswap /swap
	swapon /swap
	echo "/swap	none 	swap 	sw 0	0 >> /etc/fstab

# Setup make.conf

	MAKEOPTS="-j4 -l3"
	FEATURES="parallel-fetch buildpkg userfetch getbinpkg"
	EMERGE_DEFAULT_OPTS="--binpkg-respect-user=y"
	USE="gtkstyle qt5 tinfo X"
	LINGUAS="en_AU"
	PORTAGE_BINHOST="http://192.168.1.2:81/funtoo/packages"
	PKGDIR=/usr/portage/packages

Don't forget to also set `VIDEO_CARDS`

# Setup home crypto

/etc/init.d/crypto

	#!/sbin/openrc-run

	depend()
	{
		before localmount
		keyword -docker -jail -lxc -openvz -prefix -systemd-nspawn -vserver
	}

	start()
	{
		ebegin "Activating crypto devices"
		mount /dev/disk/by-uuid/9797-B07B /mnt/usb
		cryptsetup open -d /mnt/usb/keyfile_home /dev/disk/by-uuid/9d9c3295-534d-45b3-a16c-29ab5792abb6 luks-9d9c3295-534d-45b3-a16c-29ab5792abb6
		umount /mnt/usb
		eend 0
	}

	stop()
	{
		ebegin "Deactivating crypto devices"
		umount /home
		cryptsetup close luks-9d9c3295-534d-45b3-a16c-29ab5792abb6
		eend 0
	}

	rc-update add crypto sysinit

# Setup locale

	# echo en_AU ISO-8859-1 >> /etc/locale.gen)
	# echo en_AU.UTF-8 UTF-8 >> /etc/locale.gen)
	# locale-gen
	# echo LC_CTYPE=en_AU.UTF-8 >> /etc/env.d/02locale)
	# env-update

# Set date

	# emerge --quiet net-misc/ntp
	# date MMDDhhmmYYYY
	# ntpd -q -v
