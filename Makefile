dir = $(shell pwd)
projects = $(dir)/projects
config = $(dir)/config
scripts = $(dir)/scripts
themes = $(dir)/themes
services = $(dir)/services
other = $(dir)/other

nobeep:
	@echo "Disabling annoying system beep"
	$(shell echo "blacklist pcspkr" > sudo tee /etc/modprobe.d/nobeep.conf)
	@echo "Done"

update:
	@echo "Updating"
	sudo ego sync
	sudo epro flavor desktop
	sudo emerge -auDN --autounmask-write --quiet-build @world
	@echo "Done"

packages:
	@echo "Update USE flags"
	echo "app-text/asciidoc python_single_target_python2_7" | sudo tee -a /etc/portage/package.use
	echo "media-gfx/feh-2.21 xinerama" | sudo tee -a /etc/portage/package.use
	@echo "Installing packages required for the environment"
	sudo emerge -n --autounmask-write --quiet-build ncurses
	sudo emerge -n --autounmask-write --quiet-build arandr asciidoc awesome bash-completion compton curl cronie feh firefox-bin fish gdb gtk-engines-murrine gtk-engines-qtpixmap gtk-engines layman lxappearance lxqt-config neovim netdate pass dev-python/pip pavucontrol python qt5ct shellcheck slock sys-process/time tmux trash-cli lightdm liberation-fonts vifm wget xautolock xorg-x11 xss-lock xterm
	sudo emerge -n --autounmask-write --quiet-build gwenview konsole kde-apps/dolphin kde-plasma/systemsettings kdialog kio kio-extras ksysguard okular spectacle
	pip3 install --user pygments cheat
	sudo rc-update add alsasound boot
	sudo rc-update add cupsd default
	sudo rc-update add dbus default
	sudo rc-update add consolekit default
	sudo rc-update add metalog default
	sudo /etc/init.d/udev --nodeps restart
	sudo rc-update add xdm default
	@echo "Done"

configs:
	@echo "Copying configuration files"
	mkdir -p ~/.config/gtk-3.0
	sudo mkdir -p /root/.config/gtk-3.0
	cp --backup=t -a $(config)/awesome ~/.config/awesome
	cp --backup=t -a $(config)/fish ~/.config/fish
	cp --backup=t -a $(config)/gdb/gdbinit.d ~/.gdbinit.d
	cp --backup=t $(config)/gdb/gdbinit ~/.gdbinit
	cp --backup=t -a $(config)/nvim ~/.config/nvim
	cp --backup=t -a $(config)/qt5ct ~/.config/qt5ct
	cp --backup=t -a $(config)/qutebrowser ~/.config/qutebrowser
	cp --backup=t -a $(config)/tmux ~/.config/tmux
	cp --backup=t -a $(config)/vim ~/.vim
	cp --backup=t $(config)/gtkrc-2.0 ~/.gtkrc-2.0
	sudo cp --backup=t $(config)/gtkrc-2.0 /root/.gtkrc-2.0
	cp --backup=t $(config)/profile ~/.profile
	cp --backup=t $(config)/settings.ini ~/.config/gtk-3.0/settings.ini
	sudo cp --backup=t $(config)/settings.ini /root/.config/gtk-3.0/settings.ini
	cp --backup=t $(config)/tmux.conf ~/.tmux.conf
	cp --backup=t $(config)/vimrc ~/.vimrc
	cp --backup=t $(config)/xinitrc ~/.xinitrc
	cp --backup=t $(config)/Xresources ~/.Xresources
	echo "background=/usr/share/pixmaps/triss_portrait_wallpaper_v2_by_astoralexander-dc0be8n.jpg" | sudo tee -a /etc/lightdm/lightdm-gtk-greeter.conf
	sudo cp $(other)/Raleway /usr/fonts/Raleway
	echo "theme-name=hgwk-theme" | sudo tee -a /etc/lightdm/lightdm-gtk-greeter.conf
	echo "icon-theme-name=hgwk-icons" | sudo tee -a /etc/lightdm/lightdm-gtk-greeter.conf
	echo "font-name='Raleway Thin'" | sudo tee -a /etc/lightdm/lightdm-gtk-greeter.conf
	sudo cp $(other)/51-android.rules /etc/udev/rules.d/51-android.rules
	@echo "Done"

themes:
	@echo "Copying themes"
	sudo cp -a $(themes)/bridge /usr/share/themes/bridge
	sudo git clone https://bitbucket.org/MonkeysAreEvil/hgwk-icons.git /usr/share/icons/hgwk-icons
	sudo git clone https://bitbucket.org/MonkeysAreEvil/hgwk-theme-gtk.git /usr/share/themes/hgwk-theme
	@echo "Setting up QT/KDE"
	sudo layman -L
	sudo layman -a deepin
	sudo emerge -n --quiet-build qtstyleplugins
	kwriteconfig5 --file kdeglobals --group General --key widgetStyle gtk2
	sudo kwriteconfig5 --file kdeglobals --group General --key widgetStyle gtk2
	@echo "Done"

vim:
	@echo "Setting up vim"
	@echo "Make sure you have the tinfo use flag"
	sudo emerge -n --quiet-build --autounmask-write neovim
	pip install --user neovim==0.2.0
	pip3 install --user neovim==0.2.0
	mkdir -p ~/.cache/Nvim-R
	mkdir -p ~/.local/share/vim/undodir
	sh ~/.config/nvim/setup.sh
	@echo "Done"

launcher:
	@echo "Preparing launcher"
	git clone https://bitbucket.org/MonkeysAreEvil/hgwk-launcher.git $(projects)/hgwk-launcher
	sh $(projects)/hgwk-launcher/install.sh
	@echo "Done"

various:
	@echo "Preparing a cache for awesome logs"
	mkdir -p ~/.cache/awesome
	@echo "Done"
	@echo "Prepare a bin for tor-browser"
	sudo ln -s $(scripts)/run_browser.sh /usr/bin/tor-browser
	@echo "Done"
	@echo "Configuring fish"
	@echo "Run 'set -Ux PYTHONPATH ~/.local/lib64/python-2.7/site-packages' in fish"
	@echo "Make sure that /etc/cups/cupsd.conf has Listen 192.168.1.2:631"
	@echo "Done"

optional_packages:
	@echo "Update USE flags"
	echo "media-gfx/gmic gimp graphicsmagick" | sudo tee -a /etc/portage/package.use
	echo "app-office/calligra -crypt phonon" | sudo tee -a /etc/portage/portage.use
	echo "media-gfx/pstoedit plotutils" | sudo tee -a /etc/portage/package.use
	echo "media-libs/gegl-0.3.20 cairo" | sudo tee -a /etc/portage/package.use
	@echo "Installing additional packages"
	sudo emerge -an --quiet-build --autounmask-write audacity backintime dnscrypt-proxy ffmpeg gimp gimp-arrow-brushes gmic mpv openvpn qalculate-gtk qutebrowser dev-perl/rename rhythmbox net-vpn/tor youtube-dl
	sudo emerge -an --quiet-build --autounmask-write inkscape
	sudo emerge -an --quiet-build libreoffice-bin
	sudo emerge -an --quiet-build calligra
	sudo emerge -an --quiet-build rofi xdotool gawk pwgen
	sudo emerge -an --quiet-build --autounmask-write pandoc pandoc-citeproc aspell
	sudo emerge -an --quiet-build --autounmask-write pdfgrep poppler
	sudo emerge -an --quiet-build --autounmask-write cups gutenprint xsane
	sudo gpasswd -a crawf lp
	sudo gpasswd -a crawf lpadmin
	sudo gpasswd -a crawf scanner
	$(shell echo 192.168.1.2 >> /etc/sane.d/saned.conf)
	sudo emerge -an --quiet-build --autounmask-write texlive texlive-latexextra texlive-mathscience
	sudo emerge -an --quiet-build --autounmask-write ark k3b kate kdeconnect kdiff3 kid3 skrooge
	sudo emerge -an --quiet-build --autounmask-write dhcpcd
	sudo emerge -an --quiet-build --autounmask-write virtualbox-bin
	sudo gpasswd -a crawf vboxusers
	sudo emerge -an --quiet-build --autounmask-write google-chrome
	sudo emerge -an --quiet-build --autounmask-write app-emulation/docker docker-compose
	sudo emerge -an --quiet-build --autounmask-write simple-scan
	$(shell curl https://sh.rustup.rs -sSf > ~/rustup.sh && sh ~/rustup.sh)
	sudo layman -L
	sudo layman -a zugaina
	sudo layman -a buzden
	sudo layman -S
	sudo emerge -an --quiet-build --autounmask-write =dev-java/sbt-bin-1.0.4
	sudo emerge -an --quiet-build --autounmask-write unrar p7zip lrzip
	sudo emerge -an --quiet-build --autounmask-write sshfs
	sudo emerge -an --quiet-build --autounmask-write whois nmap
	sudo emerge -an --quiet-build --autounmask-write dev-lang/R sci-mathematics/octave sci-visualization/gnuplot
	sudo emerge -an --quiet-build --autounmask-write sbcl
	sudo layman -f -o https://raw.githubusercontent.com/dmchurch/portage-overlay/master/repositories.xml -a dmchurch
	sudo emerge -an --quiet-build --autounmask-write keybase-bin
	sudo emerge -an --quiet-build nextcloud-client
	git clone https://github.com/sharkdp/bat "$(projects)/bat"
	cd "$(projects)/bat"
	cargo install bat
	cd
	@echo "Done"


# Optional
keyboard:
	@echo "Configuring keyboard"
	#sudo sed -i 's/USE="/USE="g19 /g' /etc/portage/make.conf
	@echo "Add https://github.com/CMoH/gnome15-overlay/raw/master/overlay.xml to /etc/layman/layman.cfg in the overlays: section"
	@echo "Then, layman -S; layman -a gnome15; emerge gnome15"
	@echo "May have to download ev.py for uinput to work"
	@echo "Done"

laptop:
	@echo "Preparing laptop utilities"
	sudo emerge -a laptop-mode-tools pm-utils
	sudo rc-update add laptop_mode default
	sudo rc-update add acpid default
	@echo "Done"

steam:
	@echo "Preparing Steam"
	sudo emerge -n --quiet-build layman
	sudo layman -L
	sudo layman -S
	sudo layman -a steam-overlay
	sudo emerge -n --quiet-build steam-launcher
	sudo usermod -a -G games crawf
	@echo "Done"

services:
	@echo "Installing services"
	sudo cp $(services)/dnscrypt-proxy /etc/init.d/dnscrypt-proxy
	sudo cp $(services)/openvpn.se.frootvpn /etc/init.d/openvpn.se.frootvpn
	@echo "Done"

cleanup:
	@echo "Removing backup files"
	rm ~/**/*.~*~
	@echo "Done"

install: nobeep update packages configs themes launcher various cleanup
