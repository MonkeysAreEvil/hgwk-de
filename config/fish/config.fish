set fish_greeting ""
set fish_key_bindings fish_user_key_bindings
set XDG_CURRENT_DESKTOP kde
set -gx BAT_THEME "Solarized Flat Dark"

set -gx PYTHONPATH ~/.local/lib/python2.7/site-packages $PYTHONPATH
set -gx PYTHONPATH ~/.local/lib/python3.4/site-packages $PYTHONPATH
set -gx PYTHONPATH ~/.local/lib/python3.6/site-packages $PYTHONPATH
set -gx PATH ~/.local/bin $PATH

set -gx LESS_TERMCAP_mb \e'[01;31m'       # begin blinking
set -gx LESS_TERMCAP_md \e'[01;38;5;74m'  # begin bold
set -gx LESS_TERMCAP_me \e'[0m'           # end mode
set -gx LESS_TERMCAP_se \e'[0m'           # end standout-mode
set -gx LESS_TERMCAP_so \e'[30;43m'    	  # begin standout-mode - nice search highlighting colour
set -gx LESS_TERMCAP_ue \e'[0m'           # end underline
set -gx LESS_TERMCAP_us \e'[04;38;5;146m' # begin underline
