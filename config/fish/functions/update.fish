function update
	sudo ego sync 
	sudo layman -L
	sudo layman -S
	sudo emerge -auDUN --with-bdeps=y --autounmask-write --quiet-build @world
	rsync -avP /usr/portage/packages starfleet-command:~/packages
	rustup update
	~/.vim/setup.sh
end
