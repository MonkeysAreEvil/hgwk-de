function upgrade
	sudo emerge -auDUN --with-bdeps=y --autounmask-write --quiet-build @world
	rsync -avP /usr/portage/packages starfleet-command:~/packages
end
