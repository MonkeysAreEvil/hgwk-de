function fish_user_key_bindings
	fish_vi_key_bindings
	# Revert to old ctrl-c behaviour
	bind -M insert \cc 'commandline ""'
	bind -M insert \ce accept-autosuggestion

	fzf_key_bindings
end
