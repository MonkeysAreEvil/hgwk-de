" Load plugins for all filetypes
execute pathogen#infect('bundle/always/{}')

" Load filetype specific plugins
autocmd FileType * if isdirectory('/home/<user>/.vim/bundle/' . &ft) | execute pathogen#infect('bundle/' . &ft. '/{}') | endif

" Standard settings
filetype plugin indent on
syntax on

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
set number
set relativenumber
set hlsearch
set laststatus=2
set tabstop=4
set shiftwidth=4
set scrolloff=5

" Standard keybindings
let maplocalleader = ","

noremap <M-LeftMouse> <4-LeftMouse>
inoremap <M-LeftMouse> <4-LeftMouse>
onoremap <M-LeftMouse> <C-C><4-LeftMouse>
noremap <M-LeftDrag> <LeftDrag>
inoremap <M-LeftDrag> <LeftDrag>
onoremap <M-LeftDrag> <C-C><LeftDrag>

noremap <silent> <Up> gk
imap <silent> <Up> <C-o>gk
noremap <silent> <Down> gj
imap <silent> <Down> <C-o>gj
noremap <silent> <home> g<home>
imap <silent> <home> <C-o>g<home>
noremap <silent> <End> g<End>
imap <silent> <End> <C-o>g<End>

" Plugin keybindings
nnoremap <silent>U :UndotreeToggle<CR>

" Autosave
autocmd InsertLeave,TextChanged * if expand('%') != '' | update | endif

" Enable persistent undo
if has("persistent_undo")
	set undodir=~/.local/share/vim/undodir
	set undofile
endif

" Use pipe character (|) in insert mode in tmux
if exists('$TMUX')
	let &t_SI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=1\x7\<Esc>\\"
	let &t_EI = "\<Esc>Ptmux;\<Esc>\<Esc>]50;CursorShape=0\x7\<Esc>\\"
else
	let &t_SI = "\<Esc>]50;CursorShape=1\x7"
	let &t_EI = "\<Esc>]50;CursorShape=0\x7"
endif

" Fix various issues, such as loading python
set shell=/bin/bash

" Set colours
let $NVIM_TUI_ENABLE_TRUE_COLOR=1
set background=dark
colorscheme NeoSolarized

" Enable Python2
let g:python_host_prog="/usr/bin/python2"

" Setup completion
let g:deoplete#enable_at_startup = 1

" Set up Lisp
autocmd bufread,bufnewfile *.lisp setlocal equalprg=lispindent.lisp

" Setup pandoc syntax
augroup pandoc_syntax
	au! BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc
augroup END

" Setup plugins
let R_in_buffer = 1
let R_tmux_split = 1
let R_tmpdir = "~/.cache/Nvim-R"
let R_compldir = "~/.cache/Nvim-R"
let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
let g:syntastic_cpp_compiler_options = ' -std=c++1z'
let g:syntastic_cpp_remove_include_errors = 1
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDDefaultAlign = 'left'
let g:NERDCommentEmptyLines = 1
let g:NERDTrimTrailingWhitespace = 1
