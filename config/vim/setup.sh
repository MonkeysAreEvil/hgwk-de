#!/bin/bash

set -e

mkdir -p ~/.vim/bundle

if [ ! -d ~/.vim/autoload ]
then 
	git clone https://github.com/tpope/vim-pathogen.git ~/.vim/autoload
else
	cd ~/.vim/autoload
	git pull
fi

if [ ! -d ~/.vim/bundle/always/auto-pairs ]
then
	git clone https://github.com/jiangmiao/auto-pairs ~/.vim/bundle/always/auto-pairs
else
	cd ~/.vim/bundle/always/auto-pairs
	git pull
fi

if [ ! -d ~/.vim/bundle/always/deoplete.nvim ]
then
	git clone https://github.com/Shougo/deoplete.nvim ~/.vim/bundle/always/deoplete.nvim
else
	cd ~/.vim/bundle/always/deoplete.nvim
	git pull
fi

if [ ! -d ~/.vim/bundle/always/nerdcommenter ]
then 
	git clone https://github.com/scrooloose/nerdcommenter ~/.vim/bundle/always/nerdcommenter
else
	cd ~/.vim/bundle/always/nerdcommenter
	git pull
fi

if [ ! -d ~/.vim/bundle/always/supertab ]
then 
	git clone https://github.com/ervandew/supertab ~/.vim/bundle/always/supertab
else
	cd ~/.vim/bundle/always/supertab
	git pull
fi

if [ ! -d ~/.vim/bundle/always/syntastic ]
then 
	git clone https://github.com/scrooloose/syntastic.git ~/.vim/bundle/always/syntastic
else
	cd ~/.vim/bundle/always/syntastic
	git pull
fi

if [ ! -d ~/.vim/bundle/always/undotree ]
then
	git clone https://github.com/mbbill/undotree ~/.vim/bundle/always/undotree
else
	cd ~/.vim/bundle/always/undotree
	git pull
fi

if [ ! -d ~/.vim/bundle/always/vim-airline ]
then 
	git clone https://github.com/bling/vim-airline ~/.vim/bundle/always/vim-airline
else
	cd ~/.vim/bundle/always/vim-airline
	git pull
fi

if [ ! -d ~/.vim/bundle/always/vim-visual-increment ]
then
	git clone https://github.com/triglav/vim-visual-increment ~/.vim/bundle/always/vim-visual-increment
else
	cd ~/.vim/bundle/always/vim-visual-increment
	git pull
fi

if [ ! -d ~/.vim/bundle/markdown/vim-pandoc-syntax ]
then
	git clone https://github.com/vim-pandoc/vim-pandoc-syntax ~/.vim/bundle/markdown/vim-pandoc-syntax
else
	cd ~/.vim/bundle/markdown/vim-pandoc-syntax
	git pull
fi

if [ ! -d ~/.vim/bundle/R/Nvim-R ]
then
	git clone https://github.com/jalvesaq/Nvim-R ~/.vim/bundle/R/Nvim-R
else
	cd ~/.vim/bundle/R/Nvim-R
	git pull
fi

if [ ! -d ~/.vim/bundle/rust/rust.vim ]
then
	git clone --depth=1 https://github.com/rust-lang/rust.vim.git ~/.vim/bundle/rust/rust.vim
else
	cd ~/.vim/bundle/rust/rust.vim
	git pull
fi

if [ ! -d ~/.vim/bundle/rust/vim-racer ]
then
	git clone https://github.com/racer-rust/vim-racer ~/.vim/bundle/rust/vim-racer
else
	cd ~/.vim/bundle/rust/vim-racer
	git pull
fi

if [ ! -d ~/.vim/bundle/toml/vim-toml ]
then
	git clone https://github.com/cespare/vim-toml ~/.vim/bundle/toml/vim-toml
else
	cd ~/.vim/bundle/toml/vim-toml
	git pull
fi

if [ -f ~/.vim/autoload/pathogen.vim ]
then
    rm ~/.vim/autoload/pathogen.vim
fi
ln -s ~/.vim/autoload/autoload/pathogen.vim ~/.vim/autoload/pathogen.vim
